import React from 'react';
import css from './SiderMenu.module.css';
import { Menu, Icon, Typography } from 'antd';
import { siderMenu as string } from '../../utils/strings';
import { ReactComponent as EnergyIcon } from '../../assets/images/energy.svg';
import { ReactComponent as DemandIcon } from '../../assets/images/response.svg';
import { ReactComponent as MonthIcon } from '../../assets/images/month.svg';
import { ReactComponent as HomeIcon } from '../../assets/images/home.svg';
import LogoIcon from '../../assets/images/logo.png';
import { withRouter } from 'react-router-dom';
import { path } from '../../utils/Routes';

const { SubMenu } = Menu;
const { Text } = Typography;

const menues = {
    pTrack: {
        title: 'pTrack',
        icon: HomeIcon,
        subMenu: {
            home: {
                title: 'Home',
                path: path.pTrack.root + path.pTrack.home
            },
            dataScienceEdition: {
                title: 'Data Science Edition',
                path: path.pTrack.root + path.pTrack.datascienceEdition
            },
            admin: {
                title: 'admin',
                path: path.pTrack.root + path.pTrack.admin
            }
        }
    },
    energyMonotoring: {
        title: 'Energy Monitoring',
        icon: EnergyIcon,
        subMenu: {
            occupantEngagement: {
                title: 'Occupant Engagement',
                path: path.energyMonitoring.root + path.energyMonitoring.occupantEngagement
            },
            realTimeData: {
                title: 'Real Time Data',
                path: path.energyMonitoring.root + path.energyMonitoring.realTimeData
            },
            alarms: {
                title: 'Alarms',
                path: path.energyMonitoring.root + path.energyMonitoring.alarms
            }
        }
    },
    demandResponse: {
        title: 'Demand Response (DR+)',
        icon: DemandIcon,
        path: path.demandResponse
    },
    MonthlyPeak: {
        title: 'Monthly Peak',
        icon: MonthIcon,
        path: path.monthlyPeak
    }
};

let activeMenu = menues.energyMonotoring.subMenu.occupantEngagement.title;
let openMenu = menues.energyMonotoring.title;
let currentPath = '';


const siderMenu = (props) => {

    currentPath = props.location.pathname;

    let logoLayout = (
        <div className={css.LogoLayout}>
            <div className={css.LogoRow}>
                <img src={LogoIcon} alt={'Logo'} className={css.LogoIcon} />
            </div>
        </div>
    );

    if (!props.collapsed) {
        logoLayout = (
            <div className={css.LogoLayout}>
                <div className={css.LogoRow}>
                    <img src={LogoIcon} alt={'Logo'} className={css.LogoIcon} />
                    <Text className={css.Title}>{string.title}</Text>
                </div>
                <div className={css.WelcomeRow}>
                    <Text className={css.Welcome} >
                        {string.welcome}
                    </Text>
                    <Text className={css.Company} >
                        Some Company
                    </Text>
                </div>
            </div>
        );
    }

    const menuClick = (path) => {
        props.history.push(path);
    };

    const menuLayout =  Object.keys(menues).map(menuKey => {
        const menu = menues[menuKey];
        if (menu.subMenu) {
            return (
                <SubMenu
                    key={menu.title}
                    title={
                        <span>
                            <Icon component={menu.icon} theme="filled" />
                            <span>{menu.title}</span>
                        </span>
                    }>
                    {
                        Object.keys(menu.subMenu).map(subKey => {
                            const subMenu = menu.subMenu[subKey];
                            activeMenu = currentPath === subMenu.path ? subMenu.title : activeMenu;
                            openMenu = currentPath === subMenu.path ? menu.title : openMenu;
                            return (
                                <Menu.Item
                                    key={subMenu.title}
                                    onClick={() => menuClick(subMenu.path)}>
                                    {subMenu.title}
                                </Menu.Item>
                            );
                        })
                    }
                </SubMenu>
            );

        } else {
            activeMenu = currentPath === menu.path ? menu.title : activeMenu;
            openMenu = currentPath === menu.path ? '' : openMenu;
            return (
                <Menu.Item
                    key={menu.title}
                    onClick={() => menuClick(menu.path)} >
                    <Icon component={menu.icon} theme="filled" />
                    <span>{menu.title}</span>
                </Menu.Item>
            );
        }
    });


    return (
        <div className={css.SiderMenu}>
            {logoLayout}
            <Menu
                className={css.Menu}
                defaultSelectedKeys={[activeMenu]}
                defaultOpenKeys={[openMenu]}
                mode="inline"
                theme="dark" >

                {menuLayout}

            </Menu>
        </div>
    );
};

export default withRouter(siderMenu);