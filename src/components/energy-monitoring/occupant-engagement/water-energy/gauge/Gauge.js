import React from 'react';
import GaugeChart from 'react-d3-speedometer';
import { gauge as string } from '../../../../../utils/strings';
import {gaugeProps} from '../../../../../utils/utils';

const gauge = (props) => {

    const value = props.value;
    let gaugeText = '';

    if (value >= 25 && value < 50) {
        gaugeText = string.titles.normal;

    } else if (value >= 50 && value < 75) {
        gaugeText = string.titles.good;

    } else if (value >= 75 && value <= 100) {
        gaugeText = string.titles.exellent;
    }

    return (
        <GaugeChart
            value={value}
            currentValueText={gaugeText}
            {...gaugeProps} />
    );
}

export default gauge;