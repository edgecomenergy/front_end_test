import React from 'react';
import { ReactComponent as WaterDropIcon } from '../../../../assets/images/water-drop.svg';
import { ReactComponent as EnergyIcon } from '../../../../assets/images/energy.svg';
import DotIcon from '../../../../assets/images/dot-animated.gif';
import css from './WaterEnergy.module.css';
import { waterEnergy as string } from '../../../../utils/strings';
import Gauge from './gauge/Gauge';
import { Icon, Typography } from 'antd';
import {getRandomInteger} from '../../../../utils/utils';

const { Text } = Typography;

const waterTheme = {
    layout: [css.WaterEnergy, css.Water].join(' '),
    iconCSS: css.WaterDropIcon,
    iconComponent: WaterDropIcon,
    titleCSS: css.WaterText,
    titleText: string.waterTitle,
};

const energyTheme = {
    layout: [css.WaterEnergy, css.Energy].join(' '),
    iconCSS: css.EnergyIcon,
    iconComponent: EnergyIcon,
    titleCSS: css.EnergyText,
    titleText: string.energyTitle,
};

class WaterEnergy extends React.Component {
    state = {
        value: 100,
        isWaterTheme: true,
        theme: waterTheme
    };

    interval = null;

    componentDidMount() {
        this.interval = setInterval(() => {
            const randomValue = getRandomInteger(100,25);
            this.setState(prevState => (
                {
                    value: randomValue,
                    isWaterTheme: !prevState.isWaterTheme,
                    theme: prevState.isWaterTheme ? waterTheme : energyTheme
                }
            ));
        }, 10000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <div className={this.state.theme.layout}>
                <div className={css.TitleRow}>
                    <Icon
                        className={this.state.theme.iconCSS}
                        component={this.state.theme.iconComponent} />
                    <Text className={this.state.theme.titleCSS}>
                        {this.state.theme.titleText}
                    </Text>
                </div>
                <div className={css.Gauge} >
                    <Gauge
                        value={this.state.value} />
                </div>
                <div className={css.LiveRow}>
                    <img src={DotIcon} alt={'Dot'} className={css.DotIcon} />
                    <Text className={css.Text}>{string.live}</Text>
                </div>

            </div>
        );
    }
}
export default WaterEnergy;