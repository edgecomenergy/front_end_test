import React, { Component } from 'react';
import { ReactComponent as PointIcon } from '../../../../assets/images/pointing-right.svg';
import css from './Fact.module.css';
import { fact as string } from '../../../../utils/strings';
import { Icon, Typography } from 'antd';

const { Text } = Typography;

const facts = [...string.facts];
let index = 0;
const factsLength = facts.length;

class Fact extends Component {
    state = {
        fact: facts[index]
    };

    interval = null;

    componentDidMount() {
        this.interval = setInterval(() => {
            index = index + 1 < factsLength ? index + 1 : 0;
            this.setState({ fact: facts[index] });
        }, 15000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        return (
            <div className={css.Fact} >
                <Icon
                    className={css.PointIcon}
                    component={PointIcon} />
                <Text className={css.Title}>{this.state.fact}</Text>
            </div>
        );
    }
}

export default Fact;