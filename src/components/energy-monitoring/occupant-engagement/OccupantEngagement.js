import React from 'react';
import css from './OccupantEngagement.module.css';
import Fact from './fact/Fact';
import WaterEnergy from './water-energy/WaterEnergy';
import UtilityStstus from './utility/utility-status/UtilityStatus';
import UtilitySaving from './utility/utility-saving/UtilitySaving';
import UtilityCost from './utility/utility-cost/UtilityCost';

const occupantEngagement = (props) => {
    return (
        <div>
            <Fact />
            <div className={css.Row}>
                <WaterEnergy />
                <UtilityStstus />
            </div>
            <UtilitySaving />
            <UtilityCost />
        </div>
    );
};

export default occupantEngagement;