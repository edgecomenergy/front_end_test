import React from 'react';
import css from './UtilityStatus.module.css';
import { utilityStatus as string } from '../../../../../utils/strings';
import SavingCard from './saving-card/SavingCard';
import { Typography } from 'antd';

const { Text } = Typography;

const utilityStatus = () => {
    return (
        <div className={css.UtilityStatus}>
            <Text className={css.Title}>{string.title}</Text>
            <div className={css.Cards}>
                <SavingCard
                    type="Danger"
                    money="100"
                    desc={string.savingsAchieved} />
                <SavingCard
                    type="Success"
                    money="4000"
                    desc={string.savingsTarget} />
            </div>
        </div>
    );
};

export default utilityStatus;