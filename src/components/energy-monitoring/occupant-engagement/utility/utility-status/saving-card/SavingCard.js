import React from 'react';
import css from './SavingCard.module.css';
import { Typography } from 'antd';

const { Text } = Typography;

const savingCard = (props) => {
    return (
        <div className={[css.SavingCard, css[props.type]].join(' ')} >
            <Text>
                <span className={css.DollarSign}>$</span>
                <span className={css.Money}>{props.money}</span>
            </Text>
            <Text className={css.Desc}>{props.desc}</Text>
        </div>
    );
};

export default savingCard;