import React from 'react';
import css from './UtilityCost.module.css';
import { utilityCost as string } from '../../../../../utils/strings';
import BuildingChart from './building-chart/BuildingChart';
import { Typography } from 'antd';
import { getBuildingChartData } from '../../../../../utils/utils';

const { Text } = Typography;
const buildingChartData = getBuildingChartData(25);

const utilityCost = () => {
    return (
        <div className={css.UtilityCost}>
            <Text strong className={css.Title}>{string.title}</Text>
            <BuildingChart
                value={buildingChartData}
            />
        </div>
    );
}

export default utilityCost;