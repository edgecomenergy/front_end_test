import React from 'react';
import {
    BarChart, Bar, Cell, XAxis, Tooltip,
} from 'recharts';

const yourBuilding = 'Your Building';
const yourBuildingColor = '#ad1457';
const otherBuildingColor = '#02b59f';
const xAxisDataKey = 'building';

const rotateXAxisText = (tick) => {
    return (
        <g transform={`translate(${tick.x},${tick.y})`} >
            <text x={0} y={0} dy={10} textAnchor="end" fill={tick.payload.value === yourBuilding ? yourBuildingColor : otherBuildingColor} transform="rotate(-35)">
                {tick.payload.value}
            </text>
        </g>
    );
};

const getCells = (value) => {
    return value.map((data, index) => {
        return <Cell
            key={index}
            fill={data.building === yourBuilding ? yourBuildingColor : otherBuildingColor} />;
    })
};

const buildingChart = (props) => {
    return (
        <BarChart
            width={window.outerWidth * 80 / 100}
            height={300}
            margin={{
                top: 10, right: 10, left: 30, bottom: 50,
            }}
            data={props.value} >
            <XAxis dataKey={xAxisDataKey} interval={0} tick={(tick) => {
                return rotateXAxisText(tick);
            }} />
            <Tooltip />
            <Bar dataKey="cost">
                {getCells(props.value)}
            </Bar>
        </BarChart>
    );
}

export default buildingChart;