import React from 'react';
import css from './UtilitySaving.module.css';
import { utilitySaving as string } from '../../../../../utils/strings';
import { Typography } from 'antd';
import { ProgressBar } from 'react-bootstrap';

const { Text } = Typography;

const utilitySaving = () => {
    return (
        <div className={css.UtilitySaving} >
            <div className={css.TitleRow}>
                <Text className={css.Title}>{string.title('2019')}</Text>
                <Text className={css.Target}>{string.target('48,000')}</Text>
            </div>
            <ProgressBar
            className={css.Progress}
                striped
                now={40}
                variant="success"
                label="Achieved 4,800" />

        </div>
    );
};

export default utilitySaving;