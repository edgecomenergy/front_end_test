

export const fact = {
    facts: [
        'Did you khow that programming your thermostat can reduce 10-30% on your hydro bills?',
        'Wash laundry in cold water whenever possible.',
    ]
};

export const waterEnergy = {
    waterTitle: 'Last 24 Hours Water Use',
    energyTitle: 'Last 24 Hours Energy Use',
    live: 'Live'
};

export const gauge = {
    titles: {
        exellent: 'Exellent',
        good: 'Good',
        normal: 'Normal'
    }
};

export const utilityStatus = {
    title: 'Last Month\'s Utility Status',
    savingsAchieved: 'Savings Achieved',
    savingsTarget: 'Savings Target',
};

export const utilitySaving = {
    title: (year) => `${year} Utility Savings`,
    target: (money) => `Target: $${money}`
};

export const utilityCost = {
    title: 'Utility cost Compared to similar buildings'
};

export const siderMenu = {
    title: 'Edgecom Energy',
    welcome: 'Welcome,'
};