
export const getRandomInteger = (max = 100, min = 0) => {
    return parseInt(Math.random() * (max - min) + min);
};

export const getBuildingChartData = (dataCount) => {
    const buildingChartData = [];
    for (let i = 0; i < dataCount; i++) {
        buildingChartData.push({
            building: `Building ${i + 1}`,
            cost: getRandomInteger()
        });
    }
    buildingChartData.splice(getRandomInteger(dataCount, 0), 0, {
        building: 'Your Building',
        cost: getRandomInteger()
    });

    return buildingChartData;
};

export const gaugeProps = {
    minValue: 0,
    maxValue: 100,
    segments: 4,
    maxSegmentLabels: 0,
    width: 250,
    height: 250,
    segmentColors: ['#ff4342', '#ffc000', '#c1d208', '#35c106'],
    needleHeightRatio: 0.5,
    ringWidth: 30,
    forceRender: false,
    needleColor: '#666'
};