import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import OccupantEngagement from '../components/energy-monitoring/occupant-engagement/OccupantEngagement';

export const path = {
    root: '/',
    error404: '/404',
    pTrack: {
        root: '/p-track',
        home: '/home',
        datascienceEdition: '/data-science-edition',
        admin: '/admin'
    },
    energyMonitoring: {
        root: '/energy-monitoring',
        occupantEngagement: '/occupant-engagement',
        realTimeData: '/real-time-data',
        alarms: '/alarms'
    },
    demandResponse: '/demand-response',
    monthlyPeak: '/monthly-peak'
};

const routes = () => {
    return (
        <Switch>
            <Route path={path.root} exact render={() => (
                <Redirect to={path.energyMonitoring.root + path.energyMonitoring.occupantEngagement} />)} />
            <Route
                path={path.energyMonitoring.root + path.energyMonitoring.occupantEngagement}
                component={OccupantEngagement} />
            <Route path={path.error404} render={() => <h1>404</h1>} />
            <Redirect to={path.error404} />
        </Switch>
    );
}

export default routes;