import React, { Component } from 'react';
import css from './App.module.css';
import { Layout, Icon } from 'antd';
import SiderMenu from './components/sider-menu/SiderMenu';
import Routes from '../src/utils/Routes';

const { Sider, Content, Header } = Layout;


class App extends Component {

  state = {
    siderCollapsed: false
  };

  toggleSider = () => {
    this.setState(prevState => (
      { siderCollapsed: !prevState.siderCollapsed }
    ));
  };

  render() {
    return (
      <div className={css.App}>
        <Layout className={css.Layout}>
          <Sider
            className={css.Sider}
            width={225}
            trigger={null}
            collapsible
            collapsed={this.state.siderCollapsed} >
            <SiderMenu collapsed={this.state.siderCollapsed} />
          </Sider>
          <Layout>
            <Header className={css.Header} >
              <Icon
                className={css.MenuIcon}
                type={this.state.siderCollapsed ? 'menu-unfold' : 'menu-fold'}
                onClick={this.toggleSider} />
            </Header>
            <Content>
              <Routes />              
            </Content>
          </Layout>
        </Layout>
      </div>
    );
  }
}


export default App;
